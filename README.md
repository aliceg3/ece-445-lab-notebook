# ECE 445 Lab Notebook

## Disclaimer: Most of my entries were written in Notion since we did a lot of long lab work sessions. My writing was not very cohesive at night, so I had to transfer each individual entry over to here.

## Initial Brainstorming (8/23/22)
Objective: Create list of possible ECE 445 Project ideas
- Came up with 4 main ideas
    - Remotely adjustable aircast 
    - Plant pot that rotates and adjusts based on sun angle/position (may be too simple)
    - Phone case/power bank that recharges as you walk (vibrational energy -> conserved energy)
    - Car camera that recognizes signs and reads them aloud (accessibility device, found inspiration/need on forums)

## Initial Team Meeting (8/29/22)
Objective: Discuss everyone's ideas and (hopefully) choose project idea
- Met with Saloni and Jack in order to discuss and pick idea
- Landed on Remotely Adjustable Cast because of interest in the technology required for the project and meaningful impact on medical industry

Overall, insightful meeting and shows promising group dynamics

## RFA Composition Meeting (9/4/22)
Objective: Create RFA Outline/Draft to submit soon
- Met as a group over Zoom to write RFA (written on Notion)
- Decided on 4 different subsystems (could also translate to the modular design easily)
- Still haven't decided on purchasing an aircast, creating basic aircast with the machine shop (personally, not a fan of this option because then we are recreating a medical device without real need), or possibly using aircast that I might have at home (need to follow up on this)

Overall, productive meeting. Finished and submitted RFA

## Lab Notebook Creation (9/6/22)
Objective: Create Lab Notebook for future meetings and thoughts
- Created Lab Notebook and added past meetings/entries for record keeping

## Project Proposal Meeting (9/11/22)
Objective: Meet with Group to discuss and outline project proposal before meeting with TA
- Met with Saloni and Jack in-person at Grainger (first in-person meeting!)
- Discussed project proposal, particularly the modules in the block diagram
- Created first draft of block diagram, possibility to be more specific & color-code connections better
- Finished first part (need to clarify requirements with TA on Tuesday) and split up part 4 (I have safety section)

Overall, productive meeting and great to meet everyone in-person. We all know what needs to be done by Tuesday which is good

## Safety Section Writing (9/13/22)
Objective: Write Safety section of the project proposal
- I read the General Battery Guidelines and tried to think of all the safety risks associated with our project
- Need to have team mates read it over, but decided on 2 main risks: electrical (because we are using battery) and mechanical (because we are tightening straps and inflating air cell which could hurt the user)
- We should be good with the general safety since we all did the training and talked about how nobody can be alone in the lab, but electrical will be our primary concern since the mechanical will be mitigated by the use of pressure and force sensors on those components
- Will need to discuss with TA the electrical concerns and the mentor that was emailed about to us

Overall, good. Will need to work on the requirements section before the meeting too

## First TA Meeting (9/13/22)
Objective: Meet with TA and discuss how to go about this project outside of given deadline
- Meeting with Stasiu went well
- Definitely eye opening into how much of a commitment and challenge this project will be (especially since we don't have PCB experience)
- Decided to go to ECE Machine Shop on Friday to discuss boot construction

Overall, this project will be more complex than we initially thought. No worries then

## ECE Machine Shop meeting (9/16/22)
Objective: Meet with Machine Shop to create a plan for the boot
- Saloni, Jack, and I went to the ECE Machine Shop to discuss the boot and the different mechanisms we intend to construct (like the strap adjustment module)
- Gregg was extremely helpful with the project, especially with helping us realize that we need to get on this sooner and that there are very many different ways to construct that mechanism
- However, we need to get that boot first before we can start creating that mechanism
- I spoke to my family if we can use the boot we have at home since it is shorter (therefore straps are shorter and easier to operate with) and there is an air cell

Overall, it went super well!

## TA Meeting 2 (9/22/2022)
Objective: Meet with TA to get feedback on progress so far
- We had our second TA meeting, now with the boot
- Received feedback from our TA on the Project Proposal, will be very helpful on the Design Document
- Signed up for a Design Doc review with Prof. Fliflet and started working on the Design Document
- Looked at the ESP microcontroller we will be using and learned we can start working with it without the PCB using a board we will try to buy
- Need to visit ECE Machine Shop again soon to discuss strap adjustment now that we have the boot

Overall, went well and definitely is keeping us on the right track

## Design Document Check (9/26/2022)
Objective: Get feedback on design document
- We had our Design Document Check with Prof. Fliflet and our TA
- Went well overall. Solidifies the feasibility of our idea but was very helpful in pointing out the holes in our document and thought process for the project
- Next steps: make Design Document changes, talk to machine shop, start PCB designing early, start strap adjustment early since it is mechanically tough to figure out

Overall, went well & lots to improve. Good to know where we stand

## ECE Machine Shop Meeting 2 (9/26/2022)
Objective: Having the boot create a plan
- Spoke to machine shop again (now with boot)
- Definitely need to switch out straps & get a box for the power and microcontroller
- Probably going to be difficult to make motors removable
- Need to get motors & sensors so we can get started ASAP

Overall, created a good plan for the boot and next step is to get parts

## TA Meeting 3 (9/29/2022)
Objective: Finish PCB
- Met with Stasiu again, spoke more about PCB design and about upcoming Design Review
- Need to get started on the PCB design ASAP
- A bit nervous about the design but hopefully it goes well after meeting with TAs

Overall, PCB is new and challenging but we are making progress! This is a new situation for all of us

## Design Review Meeting (10/3/2022)
Objective: Figure out motor drivers 
- Questions:
    - HX711 does not come as a chip, only as a small PCB board type. Can we use this in conjunction with our PCB? Or should we de-solder it and then attach it on PCB? https://www.sparkfun.com/products/13879
- Answer: Should NOT desolder it!!! We found alternative motor driver PWR8833 which comes without breakout board so we can continue testing

## Research into motor system (10/4/2022)
- I did some research and found the components necessary for our board (motor drivers, ADC pins, how to connect to ESP32, etc)
- Compiled into PCB Brainstorming document I shared with team mates
- Should set us up well for the design 
    
## PCB Team Meeting (10/10/2022)
- Met as a team at Grainger to finish Circuit Schematic for PCB Design
- Found a different motor driver than from my research
- Finished schematic during meeting

## Solo PCB work [Version 1] (10/11/2022)
- I worked for 5.5 hours on the PCB v1 design
- Found and imported all footprints for components necessary from SnapEDA
- Arranged all components and tried to minimize tracks and vias on the board while still providing the best placement for things like motors and power jack placement

## PCB Design Review (10/11/2022)
- Jack and I attended the PCB design review with the design I made
- Professor Gruev was very helpful with the design! He had very good suggestions for the motor torque readings as well as suggestions regarding better organization of the wires (having all red wires go one way [horizontal] and all blue wires go the other way [vertical])
- Overall, it was a very helpful design review and helped us improve our PCB design & also showed us how to use the PCBway audit system

## Group PCB work [Version 1.1] (10/11/2022 - 10/12/2022)
- Jack and I redesigned the motor reading subsystem based on Prof Gruev's suggestions
- Saloni and I redesigned the PCB based on the new suggestions and it looks great
- Sent it over to Stasiu

## TA Meeting 4 (10/13/2022)
- Saloni and I attended our weekly meeting with Stasiu (Jack was missing for some reason)
- Spent time figuring out why our PCBway order cost $62 instead of $5! Our holes were too small in the footprints of the motor drivers DRV8833 & ESP32
- I went home and figured it out, passed the audit, and sent to Stasiu (Ordered!!)
- Very helpful TA meeting, next step is to order our parts

## Parts order (10/16/2022)
- Got on a call with Jack and Saloni and ordered most of our parts (did not order ESP32, resistors, battery pack) (lab has ESP32 that might fit our needs, have resistors, and in the meantime will use phone battery packs in order to save money where we can)
- motors were pretty expensive and heavy so planning to speak with machine shop ASAP to get that started, have been blocked by parts not being here
- Overall progressing well, but need to speed it up!!

## TA Meeting 5 (10/20/2022)
- Met as a group with Stasiu again
- identified that the ESP32s available in the ECE 445 lab are the correct ones for our project
- decided to order ESP32 testing situation so we can start writing Arduino IDE code for our motors even without the PCB fully constructed
- waiting on parts & starting to work on our inidividual papers

## TI Email (10/24/2022)
- I drafted an email to send to TI to ask for the motor drivers since ECE people did not order our motor drivers yet and need them for our PCB
- sent to Jack and Saloni to review, hopefully can get the email out by tomorrow evening
Update: talked over chat to a TI Rep on 10/25/2022, could not get those motor drivers a sample so Saloni ordered them

## UI Repo (10/25/2022)
- I started creating our user interface using GitLab and React
- There were so many issues with creating GitLab repos correctly and with React, at one points I had repo-inception where the outer repo was on GitLab and the inner React app was in another personal repo
- But made good progress with the UI (no backend) and shared with the group
- Found some JS/Bluetooth packages that will be helpful later on

## TA Meeting 6 (10/27/2022)
- Had a meeting with everyone in the group and Stasiu
- Got our parts that we ordered! Great to start 
- Jack and I stopped by the machine shop and dropped off our boot & motors to get started on!! Great times
- Jack ordered ESP32 dev kit so we can get started on the programming sooner
- Ended the meeting by deciding to *work on Individual Reports over the weekend*

## Individual Report (10/28/2022 - 11/02/2022)
- I worked on my individual report for a while
- I compiled all of the things I did and found all the references for the sources I read in my work on the PCB, strap adjustment (tolerance analysis), UI, safety, etc
- I feel that since I was involved in every part of the project it was hard to write about everything, but I focused on the parts that I had primary lead on (rather than backseat driving) and stuff I could speak to confidently

## PCB Soldering Meeting (11/02/2022)
- We met as a group in the evening to start our PCB soldering now that we had our PCBs!
- We sourced as many resistors and capacitors from the Lab so that we could practice soldering with those (since we did not have the soldering assignment to practice on)
- Jack has had the most soldering experience and did a great job introducing me and Saloni to it, I already had some in the past so this was a good review
- We also tried to connect to the ESP32 Dev Kit but the micro USB B cord was not included so we could not make progress on that during our meeting
- Overall, made good initial progress and laid out a good foundation for the coming weeks before the mock demo (so soon ahh!)

## TA Meeting 7 (11/03/2022)
- Met with Jack, Saloni, and Stasiu
- Discussed our progress and gameplan for the remainder of the course leading up to the mock demo and the final demo
- With Stasiu's help, we realized that we forgot to add the ESP32 programming circuit (did not realize before since we had not gotten the chance to play around with the DevKit or used ESP32 prior)
- Decided to redesign the PCB that day to put in an order ASAP and need to submit another order for the programming circuit parts
- Stopped by the machine shope to view the progress on the boot but the machine shop lead on our project (Glen) had COVID (metal mount on the boot attached)

## ESP32 Dev Kit Meeting (11/03/2022)
- Met in the evening with Jack and Saloni to start testing the ESP32 Dev Kit
- Ran into problems with which micro USB B cord can transmit power AND data
    - Took about an hour to figure that out as well as PORT connection between ESP32 Dev Kit and Arduino IDE
- Were able to overcome issues on my laptop and program the ESP32 (first with testing the blinking LED)

## Friday meeting, motors! (11/04/2022)
- We met Friday afternoon to continue trying to program the ESP32 and test out our NEMA 23 motors
- Saloni and I focused on building the motor driver/motor circuit while Jack continued to focus on the pressure module calculations
- Saloni and I quickly realized that our motor drivers will not work for breadboarding since they are surface mount 
- We found some other motor drivers in the ECE 445 Lab which will work (L293D, I believe)
- We spent a lot of time researching how to build the proper motor driver circuit with our 4-wire bipolar stepper motor and ESP32 since all of the information out there is for 2 or 5 wire stepper motors and Arduino Unos
- We managed to build (what we thought was the correct circuit) and decided to program the ESP32 to turn the motor
- HOWEVER, the motor was not turning, it was vibrating. As we increased the RPMs, the motor would vibrate and then turn once, which was very peculiar
- After debugging for a while, I noticed that one of our OUTPUT wires from the motor driver was connected to GND instead of the correct pin. I fixed it, and then the rotation worked! That would explain why the motor was vibrating, the double-H bridge wasn't working properly (only one) which would explain the vibration
- That was a great way to end the work week with the motor driving progress, which will allow us to breadboard our strap tightening module until we have our new PCB

## Bluetooth Meeting (11/07/2022)
- I went into the lab on Monday to work on the project, mainly with the goal of setting up a Bluetooth connection in mind
- It took about 30 minutes of research into ESP32 Bluetooth connections and tools to use to successfully set up a Bluetooth connection
- The initial problems I was running into was weird characters printing in the terminals and nothing printing in the PC terminal
    - The solution ending up changing the Baud rate in the terminals to fit the Bluetooth baud rate 115200
- I ended up downloading "Bluetooth Serial Terminal" on my PC to use to connect to the ESP32 and I was able to send messages from the PC Terminal to the Arduino Serial Terminal. Success!!
- Saloni joined after I set up the Bluetooth connection, and then we worked for a couple hours trying to utilize the Bluetooth Serial Terminal in order to receive pressure values from the sensors (which is the first step of creating the pressure module functionality we want)
- It was successful!
- Then we worked on utilizing the Bluetooth Serial Terminal to send commands to the ESP32 (which is an essential capability to trigger boot tightening/loosening), I decided to test this by turning the LED on the ESP32 on and off
- We ended up being successful in doing this by sending an "A" in the PC terminal to turn on the light and a "B" to turn off the light, great way to end the lab session

## Motor Control Meeting (11/08/2022)
- We met to work on controlling the NEMA 23 motor with the Arduino/ESP32. We used a motor driver from the lab that we found since our TI motor drivers are surface-mount only
- We used the Arduino Stepper library initiatially but that is only really controlling the speed at which we go at and how many steps we actually take. We wanted to use the AccelStepper library in order to actually accelerate while tighteneing the straps so that tightening will take less time. Personally, however, I do not think this is quite necessary because it seems to add more complication/confusion to our code (less successful) and the time it takes to tighten the strap is not one of our high-level requirements: only as long as it works
- Saloni and Jack also found a great graph that maps stepper motor RPMs/speeds to the torque provided which will be ESSENTIAL to getting the correct strap tensions for our boot!

## Bluetooth Communication Meeting (11/09/2022)
- Saloni and I were able to communicate with the ESP32 via different GATT characteristics which are part of BLE communciation
- We used BLE Serial Terminal applications on our phone in order to pair with it and read values
- We ended up being able to communicate pressure (in HEX?) to our phones with the ESP32 as server using BLE

## UI Meeting and TA Meeting 8 (11/10/2022)
- Pressure module is able to work with the wifi and the UI (NOT BLUETOOTH) on the computer! Great success, however this is a major design change
- Met with TA about our parts, current point progress, and Bluetooth obstacle. He recommends using an Android for web app but we do not have access to one
- Later in the evening, Saloni and I tried to implement a pressure sampling algorithm (kind of a filter to get the most prominent values and average them), didn't work. We tried using the Arduino timer library to get the samples but it still wasn't working 
- DROPPED OFF BOOT AT MACHINE SHOP WITH GLEN TO MOUNT MOTORS AND FINALIZE MECHANICS
![image](/IMG_4678.jpg)
boot with one motor pre-finalizing work

## Fried ESP32 Realization Meeting (11/11/2022)
- Saloni and I tested the pressure module's functionality with the UI. We were very frustrated that the pressure values were not very precise (lots of jumping betweeen 0 and 4095 with nothing in between)
- Tried to troubleshoot this by implementing the pressure sampling model from last night (had a breakthrough with using delay within a for loop). It helped a little bit (I guess, but not really)
- We asked the TA on duty and Professor Fliflet to help us, and the TA was very helpful in realizing that we had accidentally fried our ESP32 ADC pins :(
- We used a multimeter to figure out that our FSR (pressure pads) WERE working and getting the correct pressure values, the ADC pins were definitely fried

## ESP32 Programming Meeting (11/12/2022)
- We had tried to program the ESP32 prior using the programming circuit we had found online and implemented on the PCB, however in the meantime we wanted to try using the FTDI programmer breakout board we had found in the ECE 445 locker. We knew one other group was successful programming this way
- We implemented the necessary circuit to program the ESP32 while also using the ESP32 microcontroller which we have soldered wires onto. It was known as "the spider" since then based on how it fit into our breadboard in a "spider-like" manner
- While we were able to recognize the ESP32 with the computer and micro-USB B wire, we quickly realized programming it was not as simple as we thought. We developed a process of grounding and powering certain pins on the microcontroller to flash the SPI and to put the microcontroller into boot mode. We made a mental note of getting switches/buttons or transistors to make this easier for us
![image](/IMG_4708.jpg)
- We ended this meeting by being stuck in boot mode on the ESP32 microcontroller (NOT devkit) while trying to program it (good start!)
- Note: We were not able to work on the pressure module at this time becaue we had fried our ESP32 dev kit ADC pins and are waiting for new dev kits to come in

## Continued Programming ESP32 Meeting (11/13/2022)
- We continued trying to program the "spider" ESP32 microcontroller (not on the devkit). We tried so many things: the FTDI, different circuits, etc. I also brough buttons from my ECE 385 lab kit however that was still not allowing us to upload code. We were stuck in boot mode indefinitely.
- Saloni and I tried to flash the SPI using the espressif firmware in her Mac terminal. It seemed promising since THAT firmware was uploading however we were not able to upload any of our code :(
- Since we had spent hours on this, we switched gears to testing the pressure module on the new ESP32 dev kits once Jack got them
- These new dev kits did not have broken ADC pins (that we know of) so testing the pressure module with our intermittent probing algorithm actually was working well! It seemed like we did not have any more changes left to make for this subsystem of the project other than converting the values from resistance (0-4095) to actual pressure (mmHg). That can be done later though once the other subsystems are working correctly since unit conversion does not impair the functionality of our project

## New ESP32 Pins and PCB Meeting (11/14/2022)
Objective: Solder new PCB and get ready for the mock demo
- We finally received our new PCB! This one has the programming circuit so we are HOPING that this circuit is correct and will allow us to program the ESP32!! We started soldering things onto the PCB. Jack did a good job with the microcontroller and other finer components, but Saloni and I also got a lot of practice soldering on other components like resistors. Very fun
- We also troubleshooted our pressure module and motor module with the new ESP32 and realized that things were not working properly because we were using some pins on the ESP32 that were not meant to be used by us (meaning they have ADC functionality but are also used in booting so they cause problems). Once we sorted that out we were able to work our modules without hurting the ESP32! Thankfully we used the right pins on the PCB so that should work... No boot yet tho, hopefully by tomorrow
- I'm happy with where we are for the mock demo

## MOCK DEMO (11/15/2022)
Objective: Complete our mock demo and get feedback regarding our boot progress
- We got our boot back this morning! It looks great with both motors attached. I also put the pressure sensors in the sides of the boot and things are coming together with the boot, UI, pressure, and now motors! Very happy
- I think the mock demo went well, and Stasiu was very helpful in fixing the ESP32 pin connections with the reflow gun and soldering paste
- We were able to read a signal being sent to the ESP32 but still no programming :( very sad. It's getting power but now not even being recognized by Arduino. I brought my hot glue gun to help keep the micro-USB B port in place since we overlooked the placement of it unfortunately eek!
- We continued on working on the motor code now that we have both motors installed on the boot

## Motor Code and UI Work Session (11/16/2022)
Objective: Run motors simultaneously with pressure module
- Now that Saloni is gone and Jack and I have lots of pre-break assignments, we are doing individual work sessions
- I spent time trying to figure out the best way to run the motor, experimenting with how to trigger the motor to run and for how long. I'm leaning towards not using the AccelStepper library since it is more complex and doesn't allow us to run the motor at a consistent speed as easily (or even backwards).
- I am currently very concerned about why the driver keeps heating up SO much when the motor is run. Seems like we need to disable the motor driver after running it
- I ended with making the motor run intermittently (kind of like pressure probing) while running with other modules. Progress since before we had pressure/motor issues, but the next step is finding how to integrate a button onto our UI

## Solo Work (11/18/2022)
Objective: Add switch to UI to trigger motor to run
- Once again, I worked alone in the lab. I did run down Jack a Devkit and other supplies so that he can work on it at home (if he wants). My plan is to also take a dev kit home, decided not to take boot because I don't want to damage it or forget it at home before the final demo
- I spent a lot of time figuring out how to add a button to our pure HTML/CSS hosted on the ESP32 web server, and I was able to add a switch! It succesfully triggers the motor to run which is GREAT. It still has some quirks (button is not very reliable). Turning on the LED, for example, is VERY reliable and responsive. But the motor switch needs to be pressed MANY times and it extremely glitchy
![image](/IMG_5051.jpg)
- Happy with the progress though because issue seems minor

## After Break Motor Switch Meeting (11/27/2022)
- Saloni and I figured out how to make the motor start turning via the switch. I was stuck on this before break, but by changing the interval the OTHER variables probe at it fixed itself
- We were getting divide by zero errors when setting speed of stepper to 0 after turning and also getting watchdog timeout errors when the motor running code was in the HTTPGET function and the loop. Fixed it by clearing the HTTPGET call to ONLY change the motor1status
- Now our motors draw LESS current (don't over heat as much) and can be controlled by the switch, great progress!

## PCB Fixing and Two Motors Meeting (11/28/2022)
Objective: Bodge wire PCB and HOPEFULLY solve all problems
- Based on our previous testing with flashing different pins while programming the microcontroller, we realized that we had routed RX and TX incorrectly on our PCB. RX from the programmer should have gone to TX on the microcontroller and vice versa.
- Therefore I cut the traces on the PCB and we bodge wired the correct connections on the PCB in HOPES that it would solve our programming problem. We have sent probably over 50 hours trying to figure it out
![image](/IMG_5131.jpg)
- This was a group success, and we verified that data was being transmitted via RX/TX to PCB from the programming circuit. Unfortunately the Arduino was still not yet recognizing the PCB. We put this on pause in preparation for our final demo
- In hopes of getting the motor subsystem to work, I got drivers from the ECE Supply store. Unfortuantely they were not dual H-bridge drivers like the one we had been using. This meant that we resorted to stealing drivers from other teams. I do not mean that criminally. One team had an old version of their PCB which had our motor driver on it, so they gave us permission to desolder it and use it in our project if it worked. And it did which was great. We are very thankful to the poker chip dispensing group!
- After doing this, we recreated the motor driving circuit for the second motor with this new driver in hopes of both working. And they kind of did? One motor would spin well while the other one would vibrate. This was so frustrating because there were no circuit differences and the driver was NOT the issues (from testing). We later figured out it was the pins that we were using for the second motor so once we changed that both motors worked simultaneously!

Overall, getting close to being ready for the final demonstration!

## CRUNCH TIME Meeting (11/29/2022)
Objective: Finish up boot (both motors left)
- Saloni and I were able to get both motors to work! I made the code so that one switch triggers both motors to turn/tighten sequentially. This simplifies the UI for the user
- We also worked on cleaning up breadboard by removing the cables between the motors and the drivers
    - Spoke with the ECE Supply Store about cutting the fasteners and crimping them into connectors
    - Saloni and I decided to try pushing them into the breadboard directly to avoid shortening already short wires more
    - Board looks much better, cleaner, and less confusing as a result
    - We also decided to have the machine shop add a hoop to keep the bottom motor strap in place (kept sliding off which prevented correct tightening)
![image](/IMG_4709.jpg)
old breaboard
![image](/IMG_5129 (1).jpg)
new breadboard
- We were also able to test the whole flow of our boot putting on by tightening the straps and then the pressure cells! Very happy
- Next step is adding control to the motor tension via input for the doctor and bodge wiring the dev kit to the PCB to avoid breadboard.
![image](/IMG_5059.PNG)
final boot
![image](/IMG_5040.jpg)
final testing with me wearing it!!

Overall, boot works functionally completely. Last things left are aesthetic

## FINAL DEMO DAY EEK (11/30/2022)
Objective: Final demo success
- This was the day. I got there early before my group mates and carefully took all the components out. Yesterday we actually re-breadboarded on a smaller breadboard for more portability and neatness so I was thinking of somehow mounting it on the boot. But we decided in the end to not mes with the functionality of our boot. The actual function was MUCH more important than possible aesthetics
- I got some tape and taped in the pressure pads and air cells more securely. I booted up the boot and tested it once again with the pressure, motors, and the portable battery pack which we built the voltage regulating circuit for last night. Things worked GREAT
- The final demo seemed to go well. I think we answered Professor Gruev's and Stasiu's questions well and while the boot did have a hiccup (unlucky) they were understanding and seemed to have good feedback for us

Overall, happy to not have to go to the lab anymore 

## Mock Presentation Preparation (11/30/2022)
- After the final demo, I got started on the mock presentation in my other classes. I found a slide deck and created a template for all of the requirements we needed to fulfill
- I got carried away and created most of the slides for the presentation but that was good. It seems like we can talk about the high-level things well, however we should find more quantatative things for the presentation (results, research, etc)

## MOCK PRESENTATION DAY (12/1/2022)
Objective: Mock presentation success
- Mock presentation went well! I am sick but pushed through it with a mask on
- We got good feedback from the communication TA:
    - Good way of speaking without too much technical information but enough to get the point across
    - Good slides, need to remove some of the emphatic devices (too many)
    - Less fillers and to watch body language

Overall, I agree with the feedback and I think we have a good balance of technical and non-technical content

## Final Report Meeting (12/4/2022)
Objective: Divide up the final report work
- We met online as a group to divide up the sections for the final report
- Instead of using Google Docs for our report like in the past, we opted to use the Word template so that it is easier for us and also to get the correct page numbering (did not workout in the design document)
- Seems like this report will be relatively simple (not saying quick) since we have kept up with our project assignments and have our past reports to pull from

Overall, happy with the divison and aiming to get done Tuesday night

## FINAL PRESENTATION DAY (12/6/2022)
Objective: Final presentation success
- I am still under the weather and was losing my voice, but the final presentation went ok!
- I think we presented well and had a good flow
- Only thing that was a little iffy was answering the questions thrown at us. There was questions about the linearity of our force sensor (which we know), but the question on whether or not the circuit using the force sensor was linear as well kind of threw us off. I believe that it is the way we constructed it, and I think our answer was acceptable

Overall, glad to get that done with well!

## Final Report Progress (12/6/2022)
Objective: Make progress on the final report
- I worked on the final report intermitently throughout the day. I read a couple of past groups' final reports as well as the final report guidelines to get a sense of what the course staff were looking for
- Today I was able to complete the abstract, some module design process and techincal specs, as well as the cost section

Overall, good + will finish up tomorrow

## Final Report Submit (12/7/2022)
Objective: Finish the final report!
- Spent all day working on the final report in chunks. Was able to help finish sections 1, 2 4, and half of 5
- I also helped format and insert all of our citations. I think we delivered a good final report! 

Overall, great success, one more day to go for ECE 445

## Lab Checkout (12/8/2022)
Objective: Goodbye
- Last ECE 445 Diary entry
- Lab checkout went well! I was the first of my teammates and spent around 30 minutes organizing all of our boxes, lockers, cleaning the breadboards, and discussing with the TAs if I could keep the boot (spoiler: i was able to keep it without taking it apart)
- I filled out the ICES forms, and a TA informed me I could leave Stasiu's folder at the front of the room with the other TA folders so they would be delivered altogether
- Checkout was good, and it feels good to come to and end with the project. I think I learned a lot about creating a product from scratch, about testing, about failing, and also about my capabilities as an engineer. I've always shyed away from actually creating a product since it seemed so daunting, but since I consider this a success it feels good to accomplish something from scratch. I am happy with how this class went
- I am very interested in knowing what grades I have, and it is very much stressing me out

Overall, I am happy with my project success and how it all went
